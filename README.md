
Library App

Creating library App(only Backend) for Students where they can check , borrow and return the book  by themselves

Requirements
----------------------
1) Should display all books which are in stock. Out of stock books are not displayed
2) Student can search a particular book
3) Student can borrow and return books
4) Only admin can add new books to the library


Technology
-----------------------
Spring boot, Java 8, MySQL


HowTo run
----------------------
1) MySql side:
- create user 'kavitha'@'localhost' identified by 'kavitha'
- grant all on *.* to 'kavitha'@'localhost' with grant option;
- mysql -u kavitha -p
- create database book;
2) Package with maven and run the App:
- java -jar libraryApi-0.0.1-SNAPSHOT.jar
3) Use a REST client (I use postman)
- http://localhost:9090/books/getBooks  :: GET
- http://localhost:9090/books/getBook/NameOfBook/getVolume/VolumeOfBook :: GET
- http://localhost:9090/books/borrowBook/NameOfBook/getVolume/VolumeOfBook :: PUT
- http://localhost:9090/books/lendBook/NameOfBook/getVolume/VolumeOfBook :: PUT
- http://localhost:9090/books/addBook/NameOfBook/getVolume/VolumeOfBook :: POST



Features
----------------------
- Basic authentication
- Custom exceptions
- Integration testing and unit testing


