package com.library.api.integration;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.library.api.dao.BookDao;
import com.library.api.model.Book;

/**
 * @author Kavitha Integration tests
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class BookControllerIT {

	private static final ObjectMapper om = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private BookDao mockBookDao;

	@Autowired
	private ObjectMapper mapper;

	@Before
	public void init() {

	}

	@Test
	public void findByNameAndVolumeITTest() throws Exception {
		Book book = new Book(1, "bookName", "bookAuthor", 1, 5);
		when(mockBookDao.findByNameAndVolume("bookName", 1)).thenReturn(book);

		mockMvc.perform(get("/book/getBook/bookName/getVolume/1"))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(1))).andExpect(jsonPath("$.name", is("bookName")))
				.andExpect(jsonPath("$.author", is("bookAuthor"))).andExpect(jsonPath("$.volume", is(1)))
				.andExpect(jsonPath("$.numberOfCopies", is(5)));

		verify(mockBookDao, times(1)).findByNameAndVolume("bookName", 1);
	}

	@Test
	public void getAllBooksTest() throws Exception {

		List<Book> books = Arrays.asList(new Book(1, "ABC", "authorone", 1, 4), new Book(2, "DEF", "authortwo", 2, 5));

		when(mockBookDao.findTotalNumberOfBooks()).thenReturn(books);

		mockMvc.perform(get("/book/getBooks")).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk()).andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("ABC"))).andExpect(jsonPath("$[0].author", is("authorone")))
				.andExpect(jsonPath("$[0].volume", is(1))).andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].name", is("DEF"))).andExpect(jsonPath("$[1].author", is("authortwo")))
				.andExpect(jsonPath("$[1].volume", is(2)));

		verify(mockBookDao, times(1)).findTotalNumberOfBooks();
	}

	@Test
	public void addBookTest() throws Exception {
		Book book = new Book(1, "bookOne", "authorOne", 2, 3);
		when(mockBookDao.save(any(Book.class))).thenReturn(book);

		mockMvc.perform(
				post("/book/addBook").content(mapper.writeValueAsString(book)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(200));

		verify(mockBookDao, times(1)).save(any(Book.class));
	}
}
