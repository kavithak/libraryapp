package com.library.api.controller;


import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.library.api.model.Book;
import com.library.api.service.BookServiceImpl;

/**
 * @author Kavitha
 * Test class  for Book Controller
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(BookController.class)
public class BookControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private BookServiceImpl bookServiceImpl;

	ObjectMapper mapper = new ObjectMapper();

	/** controller test for fetching all books
	 * @throws Exception
	 */
	@Test
	public void getAllBooksTest() throws Exception {
		Book book = new Book(1, "ABC", "ABauthor", 1, 4);
		List<Book> books = Arrays.asList(book);

		when(bookServiceImpl.findTotalNumberOfBooks()).thenReturn(books);

		mvc.perform(get("/book/getBooks").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id", is(1))).andExpect(jsonPath("$[0].name", is("ABC")))
				.andExpect(jsonPath("$[0].author", is("ABauthor"))).andExpect(jsonPath("$[0].volume", is(1)));
	}

	/** Post method for adding new book
	 * @throws Exception
	 */
	@Test
	public void addBookTest() throws Exception {
		Book book = new Book(1, "ABC", "ABauthor", 1, 4);

		Mockito.doNothing().when(bookServiceImpl).addBook(book);

		mvc.perform(
				post("/book/addBook").content(mapper.writeValueAsString(book)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(200));
	}

	/** Put method for borrowing book
	 * @throws Exception
	 */
	@Test
	public void borrowBookTest() throws Exception {

		Book book = new Book(1, "ABC", "ABauthor", 1, 4);

		when(bookServiceImpl.borrowBooks("ABC", 1)).thenReturn(book);

		mvc.perform(put("/book/borrowBook/{name}/getVolume/{volume}", "ABC", 1).with(httpBasic("user", "user"))
				.content(mapper.writeValueAsString(book)).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(200));

	}

	/** Forbidden exception raised
	 * @throws Exception
	 */
	@Test
	public void borrowBookForbiddenTest() throws Exception {

		Book book = new Book(1, "ABC", "ABauthor", 1, 4);

		when(bookServiceImpl.borrowBooks("ABC", 1)).thenReturn(book);

		mvc.perform(put("/book/borrowBook/{name}/getVolume/{volume}", "ABC", 1).with(httpBasic("anonymous", "user"))
				.content(mapper.writeValueAsString(book)).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(401));

	}
}

