package com.library.api.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.library.api.dao.BookDao;
import com.library.api.exception.BookNotFoundException;
import com.library.api.exception.BookOutOfStockException;
import com.library.api.exception.DuplicateBookEntryException;
import com.library.api.model.Book;

/**
 * @author Kavitha Test class for Service
 */
@RunWith(MockitoJUnitRunner.class)
public class BookServiceImplTest {

	@InjectMocks
	private BookServiceImpl bookServiceImpl;

	@Mock
	private BookDao dao;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	public void findTotalNumberOfBooksTest() {
		List<Book> books = Arrays.asList(new Book(1, "AB", "AB Book", 1, 17), new Book(1, "BC", "BC Book", 1, 10),
				new Book(1, "DE", "DE Book", 1, 4));

		when(dao.findTotalNumberOfBooks()).thenReturn(books);

		List<Book> bookList = bookServiceImpl.findTotalNumberOfBooks();

		assertEquals(3, bookList.size());
		verify(dao, times(1)).findTotalNumberOfBooks();
	}

	@Test
	public void findByNameAndVolumeTest() throws BookNotFoundException {
		when(dao.findByNameAndVolume("AB", 1)).thenReturn(new Book(1, "AB", "AB author", 1, 17));

		Book book = bookServiceImpl.findByNameAndVolume("AB", 1);

		assertEquals("AB", book.getName());
		assertEquals("AB author", book.getAuthor());
		assertEquals(1, book.getVolume());
		verify(dao, times(1)).findByNameAndVolume("AB", 1);
	}

	@Test
	public void addBookTest() throws DuplicateBookEntryException {
		Book book = new Book(1, "BC", "BC Book", 1, 10);
		bookServiceImpl.addBook(book);
		verify(dao, times(1)).save(book);
	}

	@Test
	public void borrowBookTest() throws BookOutOfStockException {
		when(dao.findByNameAndVolume("AB", 1)).thenReturn(new Book(1, "AB", "AB author", 1, 17));

		Book book = bookServiceImpl.borrowBooks("AB", 1);

		assertEquals(16, book.getNumberOfCopies());
		assertEquals("AB author", book.getAuthor());
		verify(dao, times(1)).save(book);
	}

	public void borrowBookOutOfStockTest() throws BookOutOfStockException {
		when(dao.findByNameAndVolume("AB", 1)).thenReturn(new Book(1, "AB", "AB author", 0, 17));

		Book book = bookServiceImpl.borrowBooks("AB", 1);

		exception.expect(BookOutOfStockException.class);
		exception.expectMessage("Book out of stock: " + book.getName());
	}

	@Test
	public void lendBookTest() {
		when(dao.findByNameAndVolume("AB", 1)).thenReturn(new Book(1, "AB", "AB author", 1, 17));

		Book book = bookServiceImpl.lendBooks("AB", 1);

		assertEquals(18, book.getNumberOfCopies());
		assertEquals("AB author", book.getAuthor());
		verify(dao, times(1)).save(book);

	}
}
