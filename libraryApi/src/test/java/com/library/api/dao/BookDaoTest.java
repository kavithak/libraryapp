package com.library.api.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.library.api.model.Book;

/**
 * @author Kavitha
 * Dao test class
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class BookDaoTest {

	@Autowired
	private BookDao bookDao;

	@Autowired
	private TestEntityManager entityManager;

	@Test
	public void saveBookTest() {
		assertNotNull(entityManager);
		Book book = new Book(1, "ABC", "ABauthor", 2, 4);
		assertNotNull(book);
		bookDao.save(book);
		Book bookOne = bookDao.findByNameAndVolume("ABC", 2);
		assertNotNull(bookOne);
		assertEquals(bookOne.getName(), book.getName());
		assertEquals(bookOne.getAuthor(), book.getAuthor());
	}

	@Test
	public void getAllBooksTest() {
		Book book = new Book(1, "ABC", "ABauthor", 2, 4);
		bookDao.save(book);
		assertNotNull(bookDao.findAll());
	}

}
