package com.library.api.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

/**
 * @author Kavitha
 * Catching all Exceptions of the project
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(BookOutOfStockException.class)
	public ResponseEntity<ErrorDetails> bookOutOfStockException(BookOutOfStockException ex) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
				HttpStatus.EXPECTATION_FAILED.toString());
		return new ResponseEntity<>(errorDetails, HttpStatus.EXPECTATION_FAILED);
	}

	@ExceptionHandler(BookNotFoundException.class)
	public ResponseEntity<ErrorDetails> bookNotFoundException(BookNotFoundException ex) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), HttpStatus.NOT_FOUND.toString());
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(DuplicateBookEntryException.class)
	public ResponseEntity<ErrorDetails> duplicateBookEntryException(DuplicateBookEntryException ex) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), HttpStatus.BAD_REQUEST.toString());
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> globleExcpetionHandler(Exception ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}