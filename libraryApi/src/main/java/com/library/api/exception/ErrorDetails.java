package com.library.api.exception;

import java.util.Date;

/**
 * @author Kavitha
 * Parameters required to define error message
 */
public class ErrorDetails {

	private Date timestamp;
	private String message;
	private String status;

	public ErrorDetails(Date timestamp, String message, String status) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.status = status;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetails() {
		return status;
	}

	public void setDetails(String details) {
		this.status = details;
	}

}
