package com.library.api.exception;

/**
 * @author Kavitha 
 * BookNotFoundException
 *
 */
public class BookNotFoundException extends Exception {

	/**
	 * Serial version id
	 */
	private static final long serialVersionUID = 1L;

	public BookNotFoundException(String name, int volume) {
		super("Book " + name + " with volume " + volume + " not found:");
	}
}
