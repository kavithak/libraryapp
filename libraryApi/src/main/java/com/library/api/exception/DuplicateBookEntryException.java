package com.library.api.exception;

/**
 * @author Kavitha 
 * Duplicate exception
 */
public class DuplicateBookEntryException extends Exception {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	public DuplicateBookEntryException(String name, int volume) {
		super("Book " + name + " with volume " + volume + " already exist in DB");
	}

}
