package com.library.api.exception;

/**
 * @author Kavitha 
 * BookOutOfStockException
 *
 */
public class BookOutOfStockException extends Exception {

	/**
	 * Serial version ID
	 */
	private static final long serialVersionUID = 1L;

	public BookOutOfStockException(String name, int volume) {
		super("Book " + name + " with volume " + volume + " is out of stock");
	}
}
