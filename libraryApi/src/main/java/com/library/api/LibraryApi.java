package com.library.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Kavitha
 * Spring Boot starter class
 */
@SpringBootApplication
public class LibraryApi {

	public static void main(String[] args) {
		SpringApplication.run(LibraryApi.class, args);
	}

}
