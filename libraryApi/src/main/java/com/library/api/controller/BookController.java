package com.library.api.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.library.api.exception.BookNotFoundException;
import com.library.api.exception.BookOutOfStockException;
import com.library.api.exception.DuplicateBookEntryException;
import com.library.api.model.Book;
import com.library.api.service.BookServiceImpl;

/**
 * @author Kavitha 
 * Controller class for mapping requests and returning response
 *
 */
@RestController
@RequestMapping("/book")
public class BookController {
	@Autowired
	private BookServiceImpl bookServiceImpl;

	// for adding book
	@PostMapping("/addBook")
	public void addBook(@RequestBody Book book) throws DuplicateBookEntryException {
		bookServiceImpl.addBook(book);
	}

	// borrowing a book
	@PutMapping("/borrowBook/{name}/getVolume/{volume}")
	public void borrowBooks(@PathVariable(value = "name") String name, @PathVariable(value = "volume") int volume)
			throws BookOutOfStockException {
		bookServiceImpl.borrowBooks(name, volume);
	}

	// lending book
	@PutMapping("/lendBook/{name}/getVolume/{volume}")
	public void lendBooks(@PathVariable(value = "name") String name, @PathVariable(value = "volume") int volume) {
		bookServiceImpl.lendBooks(name, volume);
	}

	// finding a single book
	@GetMapping("/getBook/{name}/getVolume/{volume}")
	public Book findByNameAndVolume(@PathVariable(value = "name") String name,
			@PathVariable(value = "volume") int volume) throws BookNotFoundException {
		Book book = bookServiceImpl.findByNameAndVolume(name, volume);
		return book;
	}

	// Getting all books
	@GetMapping("/getBooks")
	public List<Book> findTotalNumberOfBooks() {
		List<Book> books = bookServiceImpl.findTotalNumberOfBooks();
		return books;
	}

}
