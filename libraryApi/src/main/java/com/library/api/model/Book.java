package com.library.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Kavitha Book Entity
 */
@Entity
@Table(name = "Book")
public class Book {
	@Id
	@GeneratedValue
	private int id;

	@Column(length = 100, nullable = false)
	private String name;

	@Column(length = 100, nullable = false)
	private String author;

	@Column(name = "volume")
	private int volume = 0;

	@Column(nullable = false)
	private int numberOfCopies;
	
	public Book() {
		super();
	}

	public Book(int id, String name, String author, int volume, int numberOfCopies) {
		super();
		this.id = id;
		this.name = name;
		this.author = author;
		this.volume = volume;
		this.numberOfCopies = numberOfCopies;
	}

	public int getNumberOfCopies() {
		return numberOfCopies;
	}

	public void setNumberOfCopies(int numberOfCopies) {
		this.numberOfCopies = numberOfCopies;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

}
