package com.library.api.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.library.api.model.Book;

/**
 * @author Kavitha Dao Interface
 *
 */
public interface BookDao extends CrudRepository<Book, Integer> {

	@Query("Select b FROM Book b WHERE b.name = :name and b.volume =:volume")
	Book findByNameAndVolume(String name, int volume);

	@Query("Select b FROM Book b WHERE b.numberOfCopies > 0")
	List<Book> findTotalNumberOfBooks();

}
