package com.library.api.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.library.api.dao.BookDao;
import com.library.api.exception.BookNotFoundException;
import com.library.api.exception.BookOutOfStockException;
import com.library.api.exception.DuplicateBookEntryException;
import com.library.api.model.Book;

/**
 * @author Kavitha
 * Service class implementing bussiness logic
 */
@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookDao dao;

	public void addBook(Book book) throws DuplicateBookEntryException {
		Book newBook = dao.findByNameAndVolume(book.getName(), book.getVolume());
		if (newBook != null)
			throw new DuplicateBookEntryException(book.getName(), book.getVolume());
		else
			dao.save(book);
	}

	public Book borrowBooks(String name, int volume) throws BookOutOfStockException {
		Book book = dao.findByNameAndVolume(name, volume);
		if (book.getNumberOfCopies() > 0) {
			book.setNumberOfCopies(book.getNumberOfCopies() - 1);
			dao.save(book);
		} else
			throw new BookOutOfStockException(name, volume);
		return book;
	}

	public Book lendBooks(String name, int volume) {
		Book book = dao.findByNameAndVolume(name, volume);
		book.setNumberOfCopies(book.getNumberOfCopies() + 1);
		dao.save(book);
		return book;
	}

	@Override
	public Book findByNameAndVolume(String name, int volume) throws BookNotFoundException {
		Book book = dao.findByNameAndVolume(name, volume);
		if (book == null)
			throw new BookNotFoundException(name, volume);
		return book;
	}

	@Override
	public List<Book> findTotalNumberOfBooks() {
		List<Book> books = new ArrayList<Book>();
		dao.findTotalNumberOfBooks().forEach(books::add);
		return books;
	}

}
