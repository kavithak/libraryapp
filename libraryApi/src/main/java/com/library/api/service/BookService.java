package com.library.api.service;

import java.util.List;

import com.library.api.exception.BookNotFoundException;
import com.library.api.model.Book;

public interface BookService {
	
	Book findByNameAndVolume(String name, int volume) throws BookNotFoundException;
	 
	List<Book> findTotalNumberOfBooks();
	

}
